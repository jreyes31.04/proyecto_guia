import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:proyecto_guia/vistas/signInPage.dart';
import 'package:proyecto_guia/widgets/wcWidgets.dart';

class WelcomePage extends StatelessWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 280,
        height: 620,
        decoration: BoxDecoration(
          color: const Color(0xFFF2E8DF),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          children: [
            Spacer(flex: 2),
            titleText("Bienvenida"),
            Spacer(flex: 1),
            subTitleText(
                "Esta es una app de prueba para experimentar con ciertos widgets en Flutter. Está inspirada en el diseño de Yasir Ahmad Noori de Dribble"),
            Spacer(flex: 2),
            const Image(image: AssetImage("assets/logo.png")),
            Spacer(flex: 2),
            largeButton(
              () {
                //Aqui irá nuestra función de Sign In
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return SignInPage();
                    },
                  ),
                );
              },
              Colors.transparent,
              "Sign In",
            ),
            Spacer(flex: 1),
            largeButton(
              () {
                //Aqui irá nuestra función
              },
              Color(0xFFF2D06B),
              "Sign Up",
            ),
            Spacer(flex: 2),
          ],
        ),
      ),
    );
  }
}
